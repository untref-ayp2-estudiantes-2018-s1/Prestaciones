package principal;

public class Test {
	
	public static void main(String[] args) {
		
		AnalisisLaboratorio hematocritos = new AnalisisLaboratorio ("Cosme Fulanito", "1/6/2018", "Test de Hematocritos", 0.5, 1.0, "Normal");
		GrupoDeEstudio leucocitos = new GrupoDeEstudio ("Cosme Fulanito", "1/6/2018", "Test de Leucocitos");
		GrupoDeEstudio hematies = new GrupoDeEstudio ("Cosme Fulanito", "1/6/2018", "Test de Hematies");
		
		leucocitos.agregarPrestacion (hematocritos);
		
		AnalisisLaboratorio prequirurgico = new AnalisisLaboratorio ("Cosme Fulanito", "1/6/2018", "Test prequirurgico", 2.0, 3.0, "Anormal");
		
		hematies.agregarPrestacion(leucocitos);
		hematies.agregarPrestacion(prequirurgico);
		
		EstudioRadiologico rxTorax = new EstudioRadiologico ("Cosme Fulanito", "1/6/2018", "Test Rx Torax", "Respirar hondo, no usar cadenitas, permanecer quieto", "Torax sano. No hay anomal�as", "Normal");
		
		EstudioCardiologico ecoDoppler = new EstudioCardiologico ("Cosme Fulanito", "1/6/2018", "Test EcoDoppler", "Respirar hondo, no usar cadenitas, permanecer quieto", "Palpitaciones", "Anormal");
		
		GrupoDeEstudio estudioCompleto = new GrupoDeEstudio ("Cosme Fulanito", "1/6/2018", "Estudio Completo");
		
		estudioCompleto.agregarPrestacion(hematies);
		estudioCompleto.agregarPrestacion(rxTorax);
		estudioCompleto.agregarPrestacion(ecoDoppler);
		
		estudioCompleto.listar();
	}

}
