package principal;

public abstract class Estudio extends Prestacion {
	
	protected String indicaciones;
	protected String informe;
	protected String clasificacion;
	
	
	public Estudio (String paciente, String fecha, String nombre, String indicaciones, String informe, String clasificacion) {
		super(paciente, fecha, nombre);
		this.indicaciones = indicaciones;
		this.informe = informe;
		this.clasificacion = clasificacion;
	}
	
	public void listar() {
		super.listar();
		System.out.println("Indicaciones: " + this.indicaciones);
		System.out.println("Informe: " + this.informe);
		System.out.println("Clasificacion: " + this.clasificacion);
	}
	

}
