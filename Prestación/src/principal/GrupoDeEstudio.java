package principal;

import java.util.List;
import java.util.ArrayList;

public class GrupoDeEstudio extends Prestacion {
	
	private List <Prestacion> estudios;
	
	public GrupoDeEstudio (String paciente, String fecha, String nombre) {
		super(paciente, fecha, nombre);
		this.estudios = new ArrayList <Prestacion>();
	}
	
	public void listar() {
		super.listar();
		for(Prestacion p: estudios) {
			p.listar();
		}
	}
	
	public void agregarPrestacion(Prestacion prestacion) {
		this.estudios.add(prestacion);
	}
	
	public void quitarPrestacion(Prestacion prestacion) {
		this.estudios.remove(prestacion);
	}

}
