package principal;

public class AnalisisLaboratorio extends Prestacion{
	
	private double valorMin;
	private double valorMax;
	private String clasificacion;
	
	public AnalisisLaboratorio(String paciente, String fecha, String nombre, double valorMin, double valorMax, String clasificacion) {
		super(paciente, fecha, nombre);
		this.valorMin = valorMin;
		this.valorMax = valorMax;
		this.clasificacion = clasificacion;
	}
	
	public void listar() {
		super.listar();
		System.out.println("Valor minimo: " + this.valorMin);
		System.out.println("Valor maximo: " + this.valorMax);
		System.out.println("Clasificacion: " + this.clasificacion);
		
	}

}
