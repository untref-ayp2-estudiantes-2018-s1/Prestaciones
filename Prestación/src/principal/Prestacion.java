package principal;

public abstract class Prestacion {
	
	protected String paciente;
	protected String fecha;
	protected String nombre;
	
	public Prestacion (String paciente, String fecha, String nombre) {
		
		this.paciente = paciente;
		this.fecha = fecha;
		this.nombre = nombre;
	}
	
	public void listar () {
		System.out.println("Paciente: " + this.paciente);
		System.out.println("Fecha: " + this.fecha);
		System.out.println("Nombre: " + this.nombre);
	}

}
